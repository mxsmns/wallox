use std::path::PathBuf;

pub fn set(filename: PathBuf) {
    panic!(
        "Setting wallpaper to {:?} on Linux is unsupported",
        filename
    );
}

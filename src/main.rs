use dotenv::dotenv;

use structopt::StructOpt;
#[cfg_attr(target_os = "windows", path = "windows.rs")]
#[cfg_attr(target_os = "macos", path = "mac.rs")]
#[cfg_attr(target_os = "linux", path = "linux.rs")]
mod wallpaper;

mod unsplash;

#[derive(StructOpt)]
struct Cli {
    #[structopt(env = "TMPDIR")]
    folder: String,
    #[structopt(env = "API_KEY")]
    api_key: String,
}

fn main() {
    dotenv().ok();
    let args = Cli::from_args();
    let wallpaper_file = unsplash::download_random_image(&args.folder, args.api_key);
    wallpaper::set(wallpaper_file);
}

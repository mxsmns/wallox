use serde::Deserialize;
use std::error::Error;
use std::fs::File;
use std::io::copy;
use std::io::Cursor;
use std::path::Path;
use std::{collections::HashMap, path::PathBuf};

#[derive(Deserialize, Debug)]
struct ImageInfo {
    id: String,
    created_at: String,
    alt_description: String,
    urls: HashMap<String, String>,
}

pub fn download_random_image(folder: &str, api_key: String) -> PathBuf {
    let image = get_random_image(api_key).expect("Unable to find a random image");
    let filename = Path::new(folder).join(format!("{}.png", image.id));
    println!("{:?}", filename);
    download_url_to_file(&image.urls["full"], &filename).expect("Unable to download image");
    filename
}

fn get_random_image(api_key: String) -> Result<ImageInfo, Box<dyn Error>> {
    let url = format!(
        "https://api.unsplash.com/photos/random?client_id={}",
        api_key
    );
    let image: ImageInfo = reqwest::blocking::get(url)?.json()?;
    Ok(image)
}

fn download_url_to_file(url: &str, filename: &Path) -> Result<(), Box<dyn Error>> {
    let mut file = File::create(filename)?;
    let response = reqwest::blocking::get(url)?;
    let mut content = Cursor::new(response.bytes()?);
    copy(&mut content, &mut file)?;
    Ok(())
}

use bindings::Windows::Win32::WindowsAndMessaging::{
    SystemParametersInfoW, SYSTEM_PARAMETERS_INFO_ACTION, SYSTEM_PARAMETERS_INFO_UPDATE_FLAGS,
};
use std::iter;
use std::{
    ffi::{c_void, OsStr},
    path::PathBuf,
};

use std::os::windows::ffi::OsStrExt;

pub fn set(filename: PathBuf) {
    windows::initialize_mta().unwrap();
    let buffer = convert_to_null_terminated_buffer(&filename.as_os_str());

    unsafe {
        SystemParametersInfoW(
            SYSTEM_PARAMETERS_INFO_ACTION::SPI_SETDESKWALLPAPER,
            0,
            buffer.as_ptr() as *mut c_void,
            SYSTEM_PARAMETERS_INFO_UPDATE_FLAGS::SPIF_UPDATEINIFILE
                | SYSTEM_PARAMETERS_INFO_UPDATE_FLAGS::SPIF_SENDCHANGE,
        );
    }
}

fn convert_to_null_terminated_buffer(input: &OsStr) -> Vec<u16> {
    input
        .encode_wide()
        .chain(iter::once(0))
        .collect::<Vec<u16>>()
}

use std::{path::PathBuf, process::Command};

pub fn set(filename: PathBuf) {
    let command_string = format!(
        r#"tell application "Finder" to set desktop picture to POSIX file "{}""#,
        filename.to_str().expect("path")
    );
    println!("Executing {}", command_string);
    Command::new("osascript")
        .arg("-e")
        .arg(command_string)
        .spawn()
        .expect("Failed to set wallpaper");
}
